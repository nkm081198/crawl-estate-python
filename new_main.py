import requests
from bs4 import BeautifulSoup as Bs
import pandas as pd
from sqlalchemy import create_engine
from selenium import webdriver
import re
import os
import time
import urllib.parse as urlparse
from urllib.parse import parse_qs, urlencode


def crawl_data(url):
    print("loading................")
    count_insert = 0

    arr_link = []
    arr_title = []
    arr_area = []
    arr_category = []
    arr_posting_date = []
    arr_expiration_date = []
    arr_price = []
    arr_unit_price = []
    arr_person_contact = []
    arr_phone_number = []
    arr_address = []
    arr_hot_news = []

    req = requests.get(url)
    if not req.ok:
        return None

    soup = Bs(req.text, "html.parser")
    target_element = soup.findAll('div', {'class': 'caption_wrap'})

    if not target_element:
        return

    for e in target_element:
        link = ""
        title = ""
        area = ""
        category = ""
        posting_date = ""
        expiration_date = ""
        price = 0.0
        unit_price = ""
        person_contact = ""
        phone_number = ""
        address = ""
        hot_news = ""  # tin thường < tin vip < tin đặc biệt

        if e.find_parent('div', {'class': 'product focus'}) is not None:
            hot_news = "tin đặc biệt"
        elif e.find_parent('div', {'class': 'product vip'}) is not None:
            hot_news = "tin vip"
        else:
            hot_news = "tin thường"

        link = e.find('a')['href']
        req_temp = requests.get(link)
        print("Link: ", link)
        soup_temp = Bs(req_temp.text, "html.parser")
        # print("soup_temp : ", soup_temp.find('div', {'class': 'the-title'}))
        if soup_temp.find('div', {'class': 'the-title'}) is not None:
            title = soup_temp.find('div', {'class': 'the-title'}).find('h1').text.strip()
            get_area = soup_temp.find('div', {'class': 'the-attr'}).find_all('li')
            for t_area in get_area:
                text_temp = str(t_area.text)
                if "tổng diện tích:" in text_temp.lower():
                    area = re.findall(r"[-+]?\d*\.\d+|\d+", text_temp.replace("Tổng diện tích:", "").replace(",", "."))[0]

            get_address = soup_temp.find('div', {'class': 'gridDesign'}).find_all('li')
            for t_address in get_address:
                text_temp = str(t_address.text)
                if "địa chỉ" in text_temp.lower():
                    address = text_temp.replace("Địa chỉ", "").strip()
                elif "tên liên lạc" in text_temp.lower():
                    person_contact = text_temp.replace("Tên liên lạc", "").strip()
                elif "điện thoại" in text_temp.lower():
                    phone_number = text_temp.replace("Điện thoại", "").strip()

            get_date_and_catefory = soup_temp.find('div', {'class': 'listOption'}).find_all('li')
            for t_date_cate in get_date_and_catefory:
                text_temp = str(t_date_cate.text)
                if "loại bất động sản" in text_temp.lower():
                    category = text_temp.replace("Loại bất động sản:", "").strip()
                elif "ngày đăng" in text_temp.lower():
                    posting_date = convert_date(text_temp.replace("Ngày đăng:", "").strip())
                elif "ngày hết hạn" in text_temp.lower():
                    expiration_date = convert_date(text_temp.replace("Ngày hết hạn:", "").strip())

            get_price = soup_temp.find('div', {'class': 'price'}).text.strip()
            if not "thỏa thuận" in get_price.lower():
                if " " not in get_price:
                    continue
                price = convert_price_page_vndiaoc(get_price)
                if not "tổng dt" in get_price.lower():
                    unit_price = get_price.split(" ")[1].strip()
                else:
                    unit_price = get_price.split(" ")[1] + " " + get_price.split(" ")[2]
                unit_price = replace_unit_price(unit_price)

            # print("=======================================================================")
            # print("Link: ", link)
            # print("Title: ", title)
            # print("Area: ", area)
            # print("Address: ", address)
            # print("Person_contact: ", person_contact)
            # print("Phone_number: ", phone_number)
            # print("Category: ", category)
            # print("Posting_date: ", posting_date)
            # print("Expiration_date: ", expiration_date)
            # print("Price: ", price)
            # print("Unit_price:", unit_price)
            # print("hot_news: ", hot_news)

            arr_link.append(link)
            arr_title.append(title)
            arr_area.append(area)
            arr_category.append(category)
            arr_posting_date.append(posting_date)
            arr_expiration_date.append(expiration_date)
            arr_price.append(price)
            arr_unit_price.append(unit_price)
            arr_person_contact.append(person_contact)
            arr_phone_number.append(phone_number)
            arr_address.append(address)
            arr_hot_news.append(hot_news)

            count_insert += 1

    engine = create_engine("mysql+pymysql://{user}:{pw}@localhost/{db}"
                           .format(user="root",
                                   pw="admin",
                                   db="db_python"))
    data = pd.DataFrame({
        'link': arr_link,
        'title': arr_title,
        'area': arr_area,
        'category': arr_category,
        'posting_date': arr_posting_date,
        'expiration_date': arr_expiration_date,
        'price': arr_price,
        'unit_price': arr_unit_price,
        'person_contact': arr_person_contact,
        'phone_number': arr_phone_number,
        'address': arr_address,
        'hot_news': arr_hot_news,
    }, dtype=[str, str, ])

    data.to_sql('test', con=engine, if_exists='append', index=False)
    print("insert success " + str(count_insert) + " record !")


def convert_price_page_vndiaoc(input_price):
    price = re.findall(r"[-+]?\d*\.\d+|\d+", input_price)
    str_zero = "{0}"

    length_of_number = 0
    app_zero = 0

    if "." in input_price:
        length_of_number = len(str(price).split(".")[1].replace("']", ""))

    if "tỷ" in input_price.lower():
        app_zero = 9 - int(length_of_number)
    elif "triệu" in input_price.lower():
        app_zero = 6 - int(length_of_number)
    elif "nghìn" in input_price.lower():
        app_zero = 3 - int(length_of_number)

    for i in range(0, app_zero):
        str_zero += "0"
    return str_zero.format(str(price[0]).replace(".", ""))


def convert_date(str_date):
    # str_date format is 08/11/1998
    arr = str_date.split("/")
    day = arr[0]
    month = arr[1]
    year = arr[2]
    return year + "-" + month + "-" + day


def replace_unit_price(unit_price):
    if "tỷ" in unit_price.lower():
        return unit_price.replace("tỷ/", "").replace("tỷ", "").replace("Tỷ/", "").replace("Tỷ", "")
    elif "triệu" in unit_price.lower():
        return unit_price.replace("triệu/", "").replace("triệu", "").replace("Triệu/", "").replace("Triệu", "")
    elif "nghìn" in unit_price.lower():
        return unit_price.replace("nghìn/", "").replace("nghìn", "").replace("Nghìn/", "").replace("Nghìn", "")


def crawl_multiple_page(url):
    arr_links = []

    options = webdriver.ChromeOptions()
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--incognito')
    options.add_argument("--start-maximized")
    # options.add_argument('--headless')
    dir_path = os.path.dirname(os.path.realpath(__file__))
    driver = webdriver.Chrome(os.path.join(dir_path, "chromedriver"), options=options)
    driver.get(url)

    # get NHÀ ĐẤT MỚI NHẤT
    crawl_data(url)

    driver.find_element_by_xpath("//select[@id='city']/option[text()='Hà Nội']").click()
    driver.find_element_by_xpath("//button[@name='btnSearch']").click()

    req = requests.get(driver.current_url)
    if not req.ok:
        print("return")

    soup = Bs(req.text, "html.parser")
    target_element = soup.findAll('div', {'class': 'pagination'})

    if not target_element:
        print("return")

    total_page = 0
    url_origin = ""
    for e in target_element:
        url_origin = e.findAll('a')[-1]['href']
        parsed = urlparse.urlparse(url_origin)
        total_page = int(parse_qs(parsed.query)['p'][0])

    url_origin = url_origin.split("?")[0] + "?"

    for i in range(1, total_page + 1):
        url_temp = url_origin + urlencode({'p': i, 'sort': 0})
        arr_links.append(url_temp)

    print("start crawl data, length = ", len(arr_links))
    count = 1
    for link in arr_links:
        print("")
        print("start crawl page " + str(count) + " - link : " + link)
        crawl_data(link)
        print("crawl done " + str(count))
        count += 1

    print("======================= DONE =======================")


if __name__ == '__main__':
    start = time.time()
    crawl_multiple_page("https://vndiaoc.com/")
    # crawl_data("https://vndiaoc.com/can-ban-ha-noi.html/?p=80&sort=0")
    end = time.time()
    print("time : " + str(end - start))
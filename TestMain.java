package com.manhnk.test;

public class TestMain {

    public static void finalTest(int soCot, int phamVi, int max) {
        int soNhan = 1;
        StringBuilder builder = new StringBuilder();

        int special = -1;
        for (int i = 1; i <= max; i++) {
            builder.append(String.format("%d * %d = %d", i, soNhan, soNhan * i));
            if (i != 1 || i != max) {
                builder.append("    ");
            }

            if (i % soCot == 0) {
                if (soNhan < phamVi) { // TH1
                    builder.append("\n");
                    soNhan++;
                    i -= soCot;
                } else { // TH2
                    builder.append("\n\n");
                    soNhan = 1;
                    special = i;
                    System.out.println("special : " + special);
                }
            } else if (i == max && soNhan < phamVi) {
                builder.append("\n");
                soNhan++;
                i = (soCot >= max) ? 0 : special;
            }

        }

        System.out.println(builder);
    }

    public static void main(String[] args) {
        finalTest(5, 5, 9);
    }

}
